%bcond_without check

# https://github.com/google/gvisor
%global goipath         gvisor.dev/gvisor
# taken from the "go" branch (for build and install runsc directly by go build in openeuler)
%global commit          2cca6b79d9f42e7a572540e11e534f70b5069a34

Name:           gvisor
Version:        20220425.0
Release:        1
Summary:        A container sandbox runtime focused on security, efficiency, and ease of use

# Upstream license specification: Apache-2.0
License:        Apache-2.0
URL:            https://github.com/google/gvisor
Source0:        https://codeload.github.com/google/gvisor/tar.gz/%{name}-%{commit}.tar.gz
Source1:        vendor.tar.gz

BuildRequires:  golang >= 1.17

%description
gVisor is an open-source, OCI-compatible sandbox runtime that provides
a virtualized container environment. It runs containers with a new
user-space kernel, delivering a low overhead container security
solution for high-density applications.

gVisor integrates with Docker, containerd and Kubernetes, making it
easier to improve the security isolation of your containers while
still using familiar tooling. Additionally, gVisor supports a variety
of underlying mechanisms for intercepting application calls, allowing
it to run in diverse host environments, including cloud-hosted virtual
machines.

%prep
%autosetup -b0 -a1 -n %{name}-%{commit}

%build
CGO_ENABLED=0 GO111MODULE=on go build -mod=vendor -o bin/runsc %{goipath}/runsc

%install
%undefine _missing_build_ids_terminate_build
install -m 0755 -vd       %{buildroot}%{_bindir}
install -m 0755 -vp bin/* %{buildroot}%{_bindir}/

%if %{with check}
%check
%endif

%files
%license LICENSE
%doc README.md AUTHORS
%{_bindir}/*

%changelog
* Fri Apr 29 2022 wangyueliang <wangyueliang@kylinos.cn> - 20220425.0-1
- Package init

